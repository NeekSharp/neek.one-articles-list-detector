﻿using CommonLibrary;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

[assembly: InternalsVisibleTo("ArticlesListDetector.Test")]

namespace ArticlesListDetector
{
    delegate void PageHandlers(WebPage page);

    public enum Probability
    {
        Not,
        MayBe,
        Probably,
        Sure
    }

    internal enum UBool
    {
        False,
        True,
        Unknown
    }

    public class ListDetector
    {

        private static readonly HashSet<string> _htmlTagsForRemoving = new HashSet<string>() { "head", "aside", "nav", "footer", "style", "hr", "br", "script", "noscript", "form" };
        private static readonly HashSet<string> _positivHtmlTags = new HashSet<string>() { "div", "article", "section", "main", "ul" };
        private static readonly HashSet<string> _positivHtmlTagsListElements = new HashSet<string> { "div", "article", "section", "li" };

        private static readonly Regex _unlikelyWordsInAttributes = new Regex("footer|banner|icon|comment|combx|community|disqus|extra|foot|remark|rss|shoutbox|sidebar|sponsor|ad-break|agegate|pagination|pager|popup|tweet|twitter|related|promo", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _badLinks = new Regex(@"&[\w]+;", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _paginationLink = new Regex(@"(\/|page=?)\d+\/?", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _positivUriWordsForCategory = new Regex("category|categories|\bcat=|tag[s]?|label|hub|topic", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _positivUriWordsForArticle = new Regex(@"post\b|\d{1,4}\/\d{1,4}\/\d{0,4}\/?|archive|\d+\.html", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _negativUriWordsForCategory = new Regex(@"20[\d]{2,8}|archive|search", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _veryNegativUriWordsForCategory = new Regex(@"author|comments|comment|feed|page|forum|subscrib|\bpost\b", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _authorsPage = new Regex(@"author|user|editor[s]?|account", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _readMoreLinks = new Regex(@"read\smore|читать\s(дальше|далее|целиком|полностью)|continue\sreading|продолжить\sчтение", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _commentSystemsKeyWords = new Regex(@"comment[s]?-area|comment[s]?-form|commentform|fb-comments|m-moot|vk_comment|disqus_thread", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _positiveClassForArticlesPreview = new Regex(@"\bpost\b|article|node", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _dateInUri = new Regex(@"[\d]{2,4}\/[\d]{2,4}\/[\d]{2,4}", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

        readonly private WebPage _page = null;

        private HtmlNode _bigestTextElement = null;
        private List<HtmlNode> _h1TagNodes = null;
        private List<HtmlNode> _h2TagNodes = null;
        private List<HtmlNode> _h3TagNodes = null;

        private Probability _probabilityByRecurringElements = Probability.Not;
        HtmlDocument _html = null;

        public ListDetector(WebPage page)
        {
            if (page == null) throw new NullReferenceException("Page must be not null.");

            _html = new HtmlDocument();
            _html.LoadHtml(page.Html);
            _page = page;
        }

        public bool IsList()
        {
            //if (_page.InternalLinks == null || _page.InternalLinks.Count == 0) return false;
            // Удаляем не нужные элементы
            RemoveNotInformativeNodes();
            
            // Смотрим вероятность того, что это список, на основе повторяющихся элемеентов страницы
            //_probabilityByRecurringElements = PageWithListProbablityByRecurringElements();

            // Вероятность по признакам
            Probability listByUri = ListProbabilityByUrls(_page.Link);
            if (listByUri == Probability.Sure) return true;

            Probability listByHtml = ListProbabilityByHtml();

            if (listByUri != Probability.Not)
            {
                if(listByHtml >= Probability.Probably)
                {
                    return true;
                }
            }

            return false;
        }

        public string InnerText()
        {
            var body = _html.DocumentNode.Descendants("body").FirstOrDefault();
            if(body == null)
            {
                return _html.DocumentNode.InnerText;
            }

            return body.InnerText;
        }

        public Probability ListProbabilityByUrls(Uri uri)
        {
            if (uri.PathAndQuery == "/")
            {
                return Probability.MayBe;
            }

            if (!HasVeryNagativeWordsInUri(uri) && !LastSegmentUriIsDigitOrDate(uri) &&!_dateInUri.IsMatch(uri.PathAndQuery))
            {
                if (HasPositiveWordsInUri(uri))
                {
                    if (!HasNagativeWordsInUri(uri))
                    {
                        return Probability.Sure;
                    }
                    else
                    {
                        if (uri.Host.Contains(".blogspot."))
                        {
                            return Probability.Sure;
                        }
                        else
                        {
                            return Probability.Probably;
                        }
                    }
                }
                else
                {
                    return Probability.MayBe;
                }
            }

            return Probability.Not;
        }

        public Probability ListProbabilityByHtml()
        {
            int articlesTagCount = ArticleTagCount();
            int readMoreLinksCount = ReadMoreLinksCount();

            if (MaxParagraphsInOneElement() < 5)
            {
                if (articlesTagCount > 6)
                {
                    return Probability.Sure;
                }

                if (readMoreLinksCount > 6)
                {
                    return Probability.Sure;
                }
            }

            _probabilityByRecurringElements = PageWithListProbablityByRecurringElements();

            if (_probabilityByRecurringElements > Probability.MayBe)
            {
                if (H1TagsContainsOrWrappedLinks().Count > 4
                    || H2TagsContainsOrWrappedLinks().Count > 4
                    || H3TagsContainsOrWrappedLinks().Count > 4)
                {
                    return Probability.Sure;
                }

                if (articlesTagCount > 2)
                {
                    return Probability.Sure;
                }

                if (readMoreLinksCount > 2)
                {
                    return Probability.Sure;
                }

                if (HasElementWithChildrenHavingPositiveAttributesForList())
                {
                    return Probability.Probably;
                }

                if (PaginationLinksCount() != 0)
                {
                    if (LinkAnchorWordsCount() < 4)
                    {
                        return Probability.Sure;
                    }
                    else
                    {
                        return Probability.Probably;
                    }
                }

                if (LinkToAuthorPagesCount() > 2)
                {
                    return Probability.Probably;
                }

                return _probabilityByRecurringElements;

            }

            return Probability.Not;

        }
        

        /// <summary>
        /// Удаляем со страницы комментарии, пустые ссылки, меню и прочее
        /// </summary>
        public void RemoveNotInformativeNodes()
        {
            List<HtmlNode> nodesToRemove = new List<HtmlNode>();

            nodesToRemove.AddRange(
                _html.DocumentNode.Descendants()
                .Where(node => node.NodeType == HtmlNodeType.Comment)
                .ToList());

            nodesToRemove.AddRange(
                _html.DocumentNode.Descendants()
                .Where(a => _htmlTagsForRemoving.Contains(a.Name))
                .ToList());

            nodesToRemove.AddRange(
                _html.DocumentNode.Descendants("a")
                .Where(a => String.IsNullOrWhiteSpace(a.InnerText) || a.GetAttributeValue("href", "").Equals("/"))
                .ToList()); 

             nodesToRemove.AddRange(
                _html.DocumentNode.Descendants("div")
                .Where(a => a.GetAttributeValue("class", "").Contains("footer") || a.Id.Contains("footer"))
                .ToList());

            // Remove or RemoveAll ?
            RemoveWithChilds(nodesToRemove);
        }

        
        #region Private methods
        // internal для тестирования
        private void RemoveWithChilds(List<HtmlNode> nodes)
        {
            foreach (HtmlNode node in nodes)
            {
                node.InnerHtml = String.Empty;
                node.Remove();
            }
        }

        private bool UBoolToBool(UBool ubool)
        {
            if (ubool == UBool.True) return true;
            else return false;
        }

        private UBool BoolToUBool(bool _bool)
        {
            if (_bool == true) return UBool.True;
            else return UBool.False;
        }

        internal List<HtmlNode> H1TagsContainsOrWrappedLinks()
        {
            if(_h1TagNodes != null)
            {
                return _h1TagNodes;
            }

            List<HtmlNode> HTags = _html.DocumentNode.Descendants("h1").ToList();

            if (HTags == null && HTags.Count == 0) return new List<HtmlNode>();
            _h1TagNodes = HTags.Where(a => a.Descendants("a")!=null || a.ParentNode.Name == "a").ToList();
            return _h1TagNodes;
        }

        internal List<HtmlNode> H2TagsContainsOrWrappedLinks()
        {
            if (_h2TagNodes != null)
            {
                return _h2TagNodes;
            }

            List<HtmlNode> HTags = _html.DocumentNode.Descendants("h2").ToList();

            if (HTags == null && HTags.Count == 0) return new List<HtmlNode>();
            _h2TagNodes = HTags.Where(a => a.Descendants("a") != null || a.ParentNode.Name == "a").ToList();
            return _h2TagNodes;
        }

        internal List<HtmlNode> H3TagsContainsOrWrappedLinks()
        {
            if (_h3TagNodes != null)
            {
                return _h3TagNodes;
            }

            List<HtmlNode> HTags = _html.DocumentNode.Descendants("h3").ToList();

            if (HTags == null && HTags.Count == 0) return new List<HtmlNode>();
            _h3TagNodes = HTags.Where(a => a.Descendants("a") != null || a.ParentNode.Name == "a").ToList();
            return _h3TagNodes;
        }

        internal bool HasElementWithChildrenHavingPositiveAttributesForList()
        {
            return _html.DocumentNode.Descendants().Any(a => a.ChildNodes.Count > 2 && a.ChildNodes.Where(b => b.Attributes.Any(c => _positiveClassForArticlesPreview.IsMatch(c.Value))).Count() > 2);
        }

        /// <summary>
        /// Есть ли на странице форма или блок комментариев
        /// </summary>
        internal bool HasScriptsForComment()
        {
            return _html.DocumentNode.Descendants().Any(a => _commentSystemsKeyWords.IsMatch(a.GetAttributeValue("class", "")));
        }

        internal int ArticleTagCount()
        {
            return _html.DocumentNode.Descendants("article").Count();
        }

        internal int MaxParagraphsInOneElement()
        {
            return _html.DocumentNode.Descendants().Max(a => a.ChildNodes.Where(b => b.Name.Equals("p", StringComparison.InvariantCultureIgnoreCase)).Count());
        }

        internal HtmlNode BigestTextElementOnPage()
        {
            if (_bigestTextElement == null)
            {

                int maxLength = 0;

                foreach (HtmlNode node in _html.DocumentNode.Descendants().Where(a => a.ChildNodes.Any(b => b.Name.Equals("p", StringComparison.InvariantCultureIgnoreCase))).ToList())
                {
                    int innerTextLength = NodeInnerTextLength(node);
                    if (innerTextLength > maxLength)
                    {
                        maxLength = innerTextLength;
                        _bigestTextElement = node;
                    }
                }

                if (_bigestTextElement == null)
                {
                    maxLength = 0;
                    foreach (HtmlNode node in _html.DocumentNode.Descendants().Where(a => a.NodeType == HtmlNodeType.Text))
                    {
                        int innerTextLength = node.InnerText.Length;
                        if (innerTextLength > maxLength)
                        {
                            maxLength = innerTextLength;
                            _bigestTextElement = node;
                        }
                    }
                }
            }

            return _bigestTextElement;
        }

        /// <summary>
        /// Количество тире в URI. Им часто заменяют пробелы, а их много в названиях статей, но не в названии категорий
        /// </summary>
        internal int DashesIsUriCount()
        {
            return _page.Link.PathAndQuery.Count(a => a.Equals('-'));
        }


        /// <summary>
        /// Количество ссылок похожих на адреса страниц авторов
        /// </summary>
        internal int LinkToAuthorPagesCount()
        {
            return _page.InternalLinks.Where(a => _authorsPage.IsMatch(a.PathAndQuery)).Count();
        }

        /// <summary>
        /// Количество ссылок для перехода к полной версии статьи
        /// </summary>
        internal int ReadMoreLinksCount()
        {
            return _page.InternalLinks.Where(a => _readMoreLinks.IsMatch(a.Title)).Count();

        }

        // TODO: Проверить работает ли
        /// <summary>
        /// Ссылки похожие на страницы большого раздела
        /// </summary>
        /// <param name="page"></param>
        internal int PaginationLinksCount()
        {
            return _page.СontinuationLinks().Count;
        }

        /// <summary>
        /// Ссылки на себя
        /// </summary>
        /// <param name="page"></param>
        internal List<Link> LinksToItself()
        {
            return _page.LinksToItself();
        }

        /// <summary>
        /// За короткое название страницы даем бонусы
        /// </summary>
        /// <param name="page"></param>
        internal int LinkAnchorWordsCount()
        {
            return  _page.Link.Title.Split().Length;
        }

        /// <summary>
        /// Содержит в url характерные для кетагорий и тегов слова
        /// </summary>
        /// <param name="page"></param>
        internal bool HasPositiveWordsInUri(Uri uri)
        {
            foreach (string segment in uri.Segments)
            {
                if (_positivUriWordsForCategory.IsMatch(segment))
                {
                    return true;
                }
            }
            return false;
        }

        internal bool HasNagativeWordsInUri(Uri uri)
        {
            return _negativUriWordsForCategory.IsMatch(uri.PathAndQuery);
        }

        internal bool HasVeryNagativeWordsInUri(Uri uri)
        {
            return _veryNegativUriWordsForCategory.IsMatch(uri.PathAndQuery);
        }

        internal int ChildPagesCount()
        {
            string parentPath = _page.Link.PathAndQuery;
            return _page.InternalLinks.Where(a => a.PathAndQuery.StartsWith(parentPath) && !a.PathAndQuery.Contains("#")).ToList().Count;
        }

        internal bool LastSegmentUriIsDigitOrDate(Uri uri)
        {
            bool result = false;

            string path = uri.Segments.Last().TrimEnd('/');
            if (path.All(a => Char.IsDigit(a) || a == '-'))
            {
                result = true;
            }

            return result;
        }

        // TODO: очень много на себя берет этот метод, переписать
        internal Probability PageWithListProbablityByRecurringElements()
        {
            // Получить ноды подходящие для поиска
            List<HtmlNode> nodesWithSimilarChildren = NodesWithSimilarChildren();
            if (nodesWithSimilarChildren == null || nodesWithSimilarChildren.Count == 0)
            {
                return Probability.Not;
            }

            var body = _html.DocumentNode.Descendants("body").FirstOrDefault();
            if (body == null)
            {
                body = _html.DocumentNode.Descendants("html").FirstOrDefault();
                if (body == null) return Probability.Not;
            }

            double bodyTextLength = NodeInnerTextLength(body);

            Dictionary<HtmlNode, double> bigestElements = new Dictionary<HtmlNode, double>();

            foreach (HtmlNode node in nodesWithSimilarChildren)
            {
                int textLength = NodeInnerTextLength(node);
                bigestElements.Add(node, textLength);
            }

            foreach (var bigestListElement in bigestElements.OrderByDescending(a => a.Value))
            {
                // Считаем длинну самого большого элемента
                double maxLengthChildElement = 0;
                foreach (HtmlNode childNode in bigestListElement.Key.ChildNodes)
                {
                    int childNodeLength = NodeInnerTextLength(childNode);
                    if (maxLengthChildElement < childNodeLength)
                    {
                        maxLengthChildElement = childNodeLength;
                    }
                }

                int childsCount = bigestListElement.Key.ChildNodes.Where(a => a.NodeType == HtmlNodeType.Element).Count();

                double limit = childsCount > 3 ? 0.4 : 0.6;

                // Если он больше или равен limit длины всего списка, то он нам не подходит.
                // Получается что какой то элемент зачительно больше всех остальных
                if (maxLengthChildElement / bigestListElement.Value > limit)
                {
                    continue;
                }

                // Сравниваем длину текста найденного элемента с похожими дочерними элементами 
                // Чем большую часть страницы он занимает, тем больше вероятность что список это ключевой элемент страницы.
                double result = bigestListElement.Value / bodyTextLength;
                if (result > 0.8)
                {
                    return Probability.Sure;
                }
                else if (result > 0.4)
                {
                    return Probability.Probably;
                }
                else
                {
                    continue;
                }
            }

            return Probability.Not;
        }

        internal int NodeInnerTextLength(HtmlNode node)
        {
            int result = 0;
            if (node == null) return 0;
            foreach (HtmlNode n in node.Descendants("#text"))
            {
                result += n.InnerText.Length;
            }
            return result;
        }

        internal string StringWithoutSpaces(string str)
        {
            StringBuilder sb = new StringBuilder(str.Length);
            for (int i = 0; i < str.Length; i++)
            {
                char c = str[i];
                if (!char.IsWhiteSpace(c))
                    sb.Append(c);
            }
            return sb.ToString();
        }

        internal List<HtmlNode> ElementsWithChildren()
        {
            List<HtmlNode> result = new List<HtmlNode>();
            List<HtmlNode> nn = _html.DocumentNode.Descendants().Where(a => 
                _positivHtmlTags.Contains(a.Name) 
                && a.HasChildNodes 
                && a.ChildNodes.Where(b => b.NodeType == HtmlNodeType.Element).Count() > 2
                && !HasUnlikelyWordsInAttributes(a)).ToList();

            foreach (HtmlNode node in nn)
            {
                int childsCount = node.ChildNodes.Where(a => a.NodeType == HtmlNodeType.Element && !HasUnlikelyWordsInAttributes(a)).Count();

                int childNodesWithLinksCount = 0;

                foreach (HtmlNode subNode in node.ChildNodes.Where(a => (_positivHtmlTagsListElements.Contains(a.Name) || a.Name.Equals("a"))))
                {
                    if (subNode.Name.Equals("a", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (subNode.Descendants("h1") != null || subNode.Descendants("h2") != null || subNode.Descendants("h3") != null)
                        {
                            childNodesWithLinksCount++;
                        }
                    }
                    else if(subNode.Descendants("a") != null)
                    {
                        childNodesWithLinksCount++;
                    }
                }

                if (childNodesWithLinksCount != 0 && ((double)childNodesWithLinksCount / (double)childsCount) > 0.7)
                {
                    result.Add(node);
                }

            }

            return result;
        }

        internal bool HasUnlikelyWordsInAttributes(HtmlNode node)
        {
            if (node.HasAttributes)
            {
                //StringBuilder attributes = new StringBuilder();
                foreach (HtmlAttribute attr in node.Attributes)
                {
                    if (_unlikelyWordsInAttributes.IsMatch(attr.Value))
                    {
                        return true;
                    }
                    //attributes.Append(attr.Value);
                    //attributes.Append(" ");
                }

                //return _unlikelyWordsInAttributes.IsMatch(attributes.ToString());
            }
            return false;
        }

        internal List<HtmlNode> NodesWithSimilarChildren()
        {
            List<HtmlNode> nodesWithChildren = ElementsWithChildren();
            if (nodesWithChildren.Count < 0) return null;

            List<HtmlNode> result = new List<HtmlNode>();
            foreach (var node in nodesWithChildren/*.Where(a => a.ChildNodes.Where(b => b.NodeType == HtmlNodeType.Element).Count() < 2)*/)
            {
                if (HasSimilarChildrens(node))
                {
                    result.Add(node);
                }
            }

            return result;
        }

        internal bool HasSimilarChildrens(HtmlNode container)
        {
            HashSet<string> nodeNames = new HashSet<string>(){ "article", "div", "p", "li", "section", "a" };
            var childs = container.ChildNodes.Where(a => a.NodeType == HtmlNodeType.Element).ToList(); ;
            int containerChilsCount = childs.Count();

            for (int i = 0; i < containerChilsCount / 2; i++)
            {
                if (!nodeNames.Contains(childs[i].Name)) continue;

                string classes = childs[i].GetAttributeValue("class", "##");
                int nodeChildCount = childs[i].ChildNodes.Count;

                if (String.IsNullOrWhiteSpace(classes))
                {
                    if (container.ChildNodes.Where(a => a.NodeType == HtmlNodeType.Element && a.Name.Equals(childs[i].Name) && a.ChildNodes.Count == nodeChildCount).Count() >= containerChilsCount * 0.6)
                    {
                        return true;
                    }
                }
                else
                {
                    HashSet<string> attrs = new HashSet<string>(childs[i].GetAttributeValue("class", "##").Split());
                    //  Если классаов у сравниваемых элементов нет, они тоже считаются равны, так как содержат ##.
                    if (container.ChildNodes.Where(a => a.Name.Equals(childs[i].Name) && attrs.Intersect(a.GetAttributeValue("class", "##").Split()).Count() != 0).Count() >= containerChilsCount * 0.6)
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }
        #endregion


    }
}
