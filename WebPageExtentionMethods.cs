﻿using CommonLibrary;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

[assembly: InternalsVisibleTo("ArticlesListDetector.Test")]

namespace ArticlesListDetector
{
    static class WebPageExtentionMethods
    {
        public static int InternalLinksCount(this WebPage page)
        {
            return page.InternalLinks.Count;
        }

        public static List<Link> LinksToItself(this WebPage page)
        {
            return page.InternalLinks.Where(a => page.Link.PathAndQuery.Equals(a.PathAndQuery,StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        //public static List<HtmlNode> H1Tags(this WebPage page)
        //{
        //    return page.Html.DocumentNode.Descendants("h1").ToList();
        //}

        //public static List<HtmlNode> H2Tags(this WebPage page)
        //{
        //    return page.Html.DocumentNode.Descendants("h2").ToList();
        //}

        //public static List<HtmlNode> H3Tags(this WebPage page)
        //{
        //    return page.Html.DocumentNode.Descendants("h3").ToList();
        //}

        /// <summary>
        /// Ссылки на другие страницы в продолжение этой /page=x , например
        /// </summary>
        public static List<Link> СontinuationLinks(this WebPage page)
        {
            List<Link> result = new List<Link>();

            string thisPagePathAndQuery = page.Link.PathAndQuery;
            foreach(Link link in page.InternalLinks)
            {
                string pathAndQuery = link.PathAndQuery;
                if(thisPagePathAndQuery.Length < pathAndQuery.Length)
                {
                    if (pathAndQuery.StartsWith(thisPagePathAndQuery.TrimEnd('/')))
                    {
                        if(Regex.Match(pathAndQuery, @"(\/|=|page)\d+").Success && !pathAndQuery.Contains("#"))
                        {
                            result.Add(link);
                        }
                    }
                }
            }

            return result;
        }
    }
}
